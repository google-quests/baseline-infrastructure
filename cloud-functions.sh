gcloud auth list
gcloud config list project

# Create the function
mkdir gcf_hello_world
cd gcf_hello_world

# Create a cloud storage bucket
gsutil mb -p qwiklabs-gcp-02-a695e53a5038 gs://cloud-functions-qwiklab-bucket

# Deploy your function
gcloud functions deploy helloWorld \
  --stage-bucket cloud-functions-qwiklab-bucket \
  --trigger-topic hello_world \
  --runtime nodejs6 # include node 8 or node 10 (--runtime nodejs8; --runtime nodejs10)
  

gcloud functions describe helloWorld

# Test the function
gcloud functions call helloWorld --data '{"message":"Hello World! From a test"}'

# View logs
gcloud functions logs read helloWorld