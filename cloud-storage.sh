gcloud auth list
gcloud config list project

# create a bucket
gsutil mb gs://qwik-storage-bucket/

# Get the image
wget --output-document ada.jpg https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Ada_Lovelace_portrait.jpg/800px-Ada_Lovelace_portrait.jpg
gsutil cp ada.jpg gs://qwik-storage-bucket
rm ada.jpg

# Download the image
gsutil cp -r gs://qwik-storage-bucket/ada.jpg .

# Copy an object to a folder in the bucket
gsutil cp gs://qwik-storage-bucket/ada.jpg gs://qwik-storage-bucket/image-folder/

# List contents of a bucket or folder
gsutil ls gs://qwik-storage-bucket
gsutil ls -la  gs://qwik-storage-bucket/ada.jpg

# Make your object publicly accessible
gsutil acl ch -u AllUsers:R gs://qwik-storage-bucket/ada.jpg

# Remove public access
gsutil acl ch -d AllUsers gs://qwik-storage-bucket/ada.jpg

# Delete objects
gsutil rm gs://YOUR-BUCKET-NAME/ada.jpg
