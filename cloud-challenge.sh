# Task 1: Create a bucket
gsutil mb gs://memories-photographs-bucket/

# Task 2: Create a Pub/Sub topic
gcloud pubsub topics create messages-topic

# Task 3: Create the thumbnail Cloud Function
thumbnail-cloud-function

# Task 4: Remove the previous cloud engineer
